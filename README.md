# Runtime SourceMod Plugin

If you enjoy SourceMod and its community or if you rely on it - please don't just be a vampire's anal cavity and help them out. Help them meet their monthly goal [here](http://sourcemod.net/donate.php) and don't be too proud to throw them some pocket change if that's all you got.

Thanks :)

## License

Runtime a SourceMod L4D2 Plugin
Copyright (C) 2016  Victor B. Gonzalez

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

## About

This is a very simple plugin meant for tracking round times in Left 4 Dead 2. Everyone swears they know how to play the game but we all know deep down inside the proper way to play it is how most of us would handle a real word apocalypse scenario. We'd all end up dead out the gate. But since it's only a game, let's have some fun, let's run!

Run responsibly. Don't cry. Run!

## Usage

Runtime starts tracking automatically when a player moves. The moment Runtime detects that a player has moved, it'll begin its tracking for that round. If a round is reset for any reason, the tracked time for that round will also reset. Basing the activation of the timer on a players movement has been done to insure the widest compatibility with all custom campaigns. Some campaigns will allow you to move during a scene and this is not considered a bug (I'm looking at you Death Toilet Maze).

### Commands

```
runtime blame       // Who moved first?
runtime [ now ]     // Shows the current time
runtime *           // Lists all round times
runtime [ N | -N ]  // Show times for round N (-N is reversed)
runtime -rnow       // Resets the current round
runtime -rall       // Resets all tracked times
```

The **-rnow** and **-rall** switches fires up a vote so not just anyone can reset the times without the majority of the team consenting to the reset.

## Thanks

Thank you to **Coleo** for writing up the first Speedrun plugin that got me into SourcePawn. In just about 2 or so hours you wrote up a full working example that not only opened up my eyes up to the goodness, it got me hooked. I remember in passing I mentioned a plugin I wish existed and you made it exist. Although Runtime is a complete and total rewrite of the original, I'll never forget the good you've sparked in me with it :)

Thank you Coleo!

## Reaching out to me

I love L4D2, developing, testing and running servers more than I like playing the game. Although I do enjoy the game and it is undoubtedly my favorite game, it is the community I think I love the most. It's always good to meet new people with the same interest :)

- [My Steam Profile](http://steamcommunity.com/id/buckwangs/)
- [My Runtime GitLab Page](https://gitlab.com/vbgunz/Runtime)
- [My SourceMod Thread](https://forums.alliedmods.net/showthread.php?t=288686)

## Bugs

- Nunya
